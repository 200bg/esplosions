
module.exports = function (grunt) {
    grunt.initConfig({
        'coffee': {
            options: {
                sourceMap: true
            },
            dist: {
                files: {
                    'js/easypeasy.js': 'src/js/easypeasy.coffee',
                    'js/esplosions.js': 'src/js/esplosions.coffee',
                }
            }
        },
        'jade': {
            dist: {
                files: {
                    'index.html': 'src/index.jade',
                }
            }
        },
        connect: {
            server: {
                options: {
                    port: 8000,
                    useAvailablePort: true,
                    base: '.',
                    keepalive: false,
                }
            },
        },
        watch: {
          scripts: {
            files: [
                'src/js/*.coffee',
                'src/*.jade',
            ],
            tasks: ['coffee', 'jade'],
            options: {
              spawn: false,
            },
          },
        },
    });
    
    grunt.loadNpmTasks('grunt-contrib-jade');
    grunt.loadNpmTasks('grunt-contrib-coffee');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');

    grunt.registerTask('default', ['coffee', 'jade']);
    grunt.registerTask('server', ['connect', 'watch']);
    // grunt.registerTask('default', ['6to5', 'uglify']);

};